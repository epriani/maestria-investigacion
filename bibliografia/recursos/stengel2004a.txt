---------------------------------------- Extensión de 100 c.----------------------------------------

# Introducción

El término de PI es del siglo XX.
  - Abarca cuatro áreas:
    1. Marcas.
    2. Patentes.
    3. Derechos de autor.
    4. Secretos comerciales.
    
El concepto es bien conocido a través de la historia.
  - A partir del siglo XVI encontramos DPI.
    * Inglaterra (derechos de autor).
    * Venecia (patentes).
    
    => No se crearon para beneficiar a alguien, sino como medio de controlar bienes.
    
# PI en la historia

La protección era una cuestión moral o religiosa.
  - Culturas antiguas:
    * Ghana: la monarquía custodiaba los derechos de autor sobre nuevos diseños de textiles.
  - Los signos religiosos contemplan:
    1. Identificación.
    2. Distinción.
    
    => Mismos requerimentos para las marcas.
  - Los secretos religiosos también son uno de los primeros bienes de PI.
  - La protección de obras cabe remontarse a la Antigua Grecia.
    * Plagio como cuestión moral.
    
Un paso importante fue la constitución de DPI.
  - En los derechos de autor fue la introducción de la imprenta.
    * En el siglo XVIII toma la forma que ahora conocemos.
    
# Características de la PI

Delinear las características de lo que ahora se entiende por PI.
  - Una teoría general de la propiedad puede fallar en el intento.
  
## Aspectos económicos

Los argumentos utilitaristas se enfocan en las consecuencias de la titularidad
hacia los bienes intelectuales.
  - Desde el siglo XVIII ya se reconoce la importancia de la P literaria 
  para el progreso cultural.
  - La primera formulación de la importancia de la PI para el progreso se
  encuentra en Schumpeter.
  - PI como incentivo para crear algo socialmente beneficioso.
  - PI como recompensa al esfuerzo del creador.
  - PI como compensación al creador por su creación y no solo por el tiempo
  y dinero invertido.
  
    Antes de la creación | Después de la creación | Durante la compartición
    ---------------------|------------------------|------------------------
          incentivo      |       recompensa       |      compensación

  => Sin esto el creador recurriría a otros sistemas como los secretos comerciales.
    - La sociedad pierde.
    
## Aspectos éticos

DPI muy restrictivos pueden determinar el desarrollo cultural.
  - PI siempre ha causado sospechas.
  
  => Puede argumentarse que PI beneficia a la sociedad.
  
Toda PI son experiencias humanas a través del tiempo. => Han de compartirse.
  - En las patentes se ha de analizar el balance positivo o negativo de 
  los monopolios para la cultura.
  - Las cuestiones más delicadas se refieren a la genética.
    * Apenas es el inicio del reto.
    
## Diferencias entre la P y la PI

La información es diferente

               PF                 |                 PI
----------------------------------|-------------------------------------
Objetos tangibles                 | Objetos abstractos
Relacionado con algo real         | Relacionado a sí mismo (información)
Límites definidos                 | Límites ambiguos
Sin dicotomía                     | Dicotomía idea/expresión
Tiempo indefinido                 | Tiempo limitado
Permanece como derecho personal   | Se convierte en dominio público
Interés financiero                | Interés personal y financiero

=> Las puras teorías económicas no pueden esclarecer la diferencia.
  - Quizá una aproximación filosófica sí.
    Facilidad de control | Dificultad de control
    ---------------------|----------------------
         Rivalizan       |   No rivalizan
         
# PI en filosofía

No hay una teoría general de la PI
  - Argumentos fuertes a la P divergen cuando se aplican a la PI.
    * Las reglas de PI se han desarrollado de manera ad hoc en los tribunales
    
    => Sin niguna concepción en específico.
    
    => Desarrollo empírico desde una perspectiva económica.
    
  => Autores que lidian con PI:
    - Kant.
    - Hegel.
    - Fichte.
    - Herder.
    - Foucault.
    - Locke.
      * Intentó teoría general que incluye PI.
    - Marx.
      * Intentó teoría general que incluye PI.
    - Serran
    
    => Tal vez no es necesaria una teoría específica.
      - Siempre y cuando una teoría general pueda explicar la PI de manera
      satisfactoria.
      - Algunos autores desarrollaron argumentos de PI,
      otros queda bajo la responsabilidad de los intérpretes.
      
## Locke

Derecho natural de la PI
  Ley natural que proclama  => Propiedad privada que
  el bien común.               confiere el Estado.
  
Ilustra cómo se obtienen PI
  - Predomina en las DPI.

Mezcla la creatividad divina con la creatividad humana
  - Dios ha dado el mundo al hombre como bien común.
    * No solo para supervivencia.
    * Para su desarrollo y conveniencia.
  - El único dominio privado inicial es el del cuerpo.
  - La mezcla del esfuerzo físico con el bien común da como origen a la propiedad.
    * El trabajo refina a los objetos naturales.
    * El trabajo añade un valor a los objetos naturales.
  - Dos condiciones:
    1. Apropiación siempre y cuando se deje o haya lo suficiente e igual
    de bueno para los demás.
    2. El desperdicio está prohibido.
    
    => Disminuidos en la economía monetaria, aunque no inválidos.
    
### PI

La teoría de la P puede aplicarse a la PI.
  - El trabajo como fundamento para la apropiación.
    * La pregunta es si el trabajo es necesario para PI.
    
      => Unos consideran que es un trabajo más sencillo al físico.
      
      => Unos consideran que pensar no es trabajar.
      
    => Locke ve al trabajo como algo poco placentero, una actividad que 
    se lleva a cabo solo por la expectativa de beneficios.
      - Ocasiona que se niegue la apropiación si el trabajo involucrado
      fue placentero.
        * Quizá no hay que ser tan literales.
        
        => Trabajo como menos estrecho.
          - Esfuerzo involucrado para expresar la idea.
            * No es simplemente pensarlo.
            
          => Tal vez todavía no es suficiente para PI.
            - Para Locke el trabajo añade valor.
              * Valor = Utilidad para el hombre.
              
                => Difícil de determinar.
                
                => No toda labor intelectual es útil.
                
              => Dos aspectos:
			    1. Normativo (Locke)
			    2. Instrumental (Utilitaristas)
			      - Valioso y en general incrementa el valor social a
			      comparación de que no existiera.
			      
La condición de suficiente e igual de bien también se ha considerado.
  - Que todos puedan extraer la misma cantidad.
  - No es necesario un bien común infinito sino prácticamente inagotable.
  
  => Con las ideas se trata de una condición fácil de conseguir.
    - Puede controlarse la expresión, pero no la idea.
      * Todos pueden tener de nuevo la idea => el bien común no se agota.
      
Hughes dice que hay ideas que no pueden quitarse del bien común.
  * Ideas de todos los días.
  
    => vs: ya existe en el mundo y en su apropiación reside una búsqueda
    de especificidad que involucra trabajo.
  
  * Ideas extraordinarias.
  
    => vs: describen lo que ya forma parte del mundo.
    
Otro elemento importante es la condición del no desperdicio.
  - Se da en el contexto de desperdicio de comida.
    * En PI no hay desperdicio porque, en su protección, se hace pública.
    
      => Es muy difícil un intercambio entre la moneda y la idea.
      
      => Es posible un acceso a la idea mediante el acceso de su expresión
      a través de la moneda.
      
        => La condición solo abarca a objetos tangibles.
        
  => Aún se destruya lo tangible, por el carácter público de los DPI
  no existe desperdicio.
  
### Sumario

Locke es el fundamento de las DPI.
  - Solo da ideas abstractasy generales.
  - Su resultado es una interpretación.
  - Locke rehuye de leyes morales.
  
## Kant

Kant desarolló una filosofía del derecho que incluye una teoría de la P;
así como habló de la PI.
  - «Science of Right» (1796).
  - «Of the Injustice of Counterfeiting Books» (1785).
  
DP como derecho privado
  - Basado en el derecho innato de la libertad.
    * Única posesión prejurídica, D personal.
    * Incluye el D de acción común.
    
      => Hacer a través de otros sin infringir sus derechos.
      
        => Posibilidad de apropiación.
        
    => El asentimiento *a priori* permite la apropiación de la voluntad.
      - Algo es «mío» cuando existe un nexo con una forma en específico.

Todas las demás apropiaciones son externas y adquiridas => D real.
  - Apropiación original o primaria sino requiere de otras personas.
  
### PI

Hay una consideración especial en la PI.
  - Libro: escrito que contiene una disertación que alguien ofrece al
  público a través de signos visibles del discurso.
  
  => Llave para entrar al sistema kantiano de DA.
    - El autor tiene derechos sobre su discurso, no sobre el libro en sí.
      Derecho a discurso != D de copia
      ------------------    ----------
      Objeto ideal          Substrato material
       
    => Predecesor de la noción actual de PI.
    
No es claro cómo el autor adquiere el derecho de su discurso.
  - Todo es adquisición externa, incluso el discurso del autor.
  - Todo autor tiene el derecho innato de prevenir a otros que hablen en su nombre.
  
  => Confusión si se trata de un D personal o D real.
  
Para buscar una solución, se emplea la dicotomía *operam*/*opus*
  - *Operam*: esfuerzo del autor (*working*).
  - *Opus*: lo terminado que puede ser alienado (*work*).
  
  => Creación del discurso => inalienable.
  
El concepto del autor es central.
  - Kant limita DPI a DA.
    * En las demás obras artísticas hay presentación directa.
    
    => No tienen un pensamiento oculto.
    
  => Bebe del sentido moderno de «autor» del romanticismo alemán.
    - Autor como:
      * Creador activo.
      
        => Schlegel: trabajo de un hombre, propiedad de su mente.
        
      * Sobresale a otro tipo de artistas != superioridad.
      
        => Herder: en el libro hay parte del alma del autor.
        
DA = DPI
- Resto de posibles DPI no presentan una forma de discurso.
  * Puerta trasera para las marcas y secretos comerciales.
    - Kant nunca habló de ellos.
    - PI es un término del siglo XX.
    
    => Búsuqeda de una vinculación.
      - Quizá Kant no estaría de acuerdo.
      
DA:
  - Relación inalienable entre el autor y su obra.
  - Prohibir la impresión.
  - Otro puede basarse hasta hacerlo parte de su discurso.
    * Como la traducción.
    
  => DA syss el discurso permanece sin cambios.
    - Siempre cargan un pensamiento en específico.
    
### Sumario

Kant no explica las caracterísitcas actuales del copyright como
  - la existencia limitada,
  - el dominio público,
  - la distinción entre idea y expresión,
  - las circunstancias en las que un discurso es uno nuevo.
    * Solo requiere pequeños cambios.
    
  => DPI reducido a DA.
  
## Michel de Servan

Mencionó aspectos novedosos de la PI.
  - «Réflexions sur le COnfessions de J-J Rousseau» (1783).
  - «Commentaire sur un passage de livre de M. Necker» (1784).
  
  => Solo se enfoca a lo literario.
    * ¿Por qué las letras son P del autor?
    
      => La expresión ya forma parte del autor.
      
    => Contexto de correspondencia epistolar y su derecho a la privacidad.
    
      => Introducción a la noción de coautoría.
        - El receptor es autor, aún sin dar respuesta epistolar por el
        motivo que el emisor lo tiene en mente.
        
        => Relación contextual del DPI.
          - La identidad de una persona depende de su relación con el mundo.
            * Quien influya en el trabajo, también es propietario.
            * DPI si hay relación con D a la libertad.
            * Justificación del D limitado a la PI en cuanto tiempo.
            
DA como quien está involucrado, no quién lo creó.
  - Más allá del concepto contemporáneo del autor.
  - Combinación de elementos utilitarios y teoría personalista.
  
  => No es tan sofisticado pero muestra aspectos importantes.
  
## Hegel

Basado en su filosofía del D
  - Reacción ante la insatisfactoria postura del D natural.
  
### Bosquejo básico de la teoría hegeliana

Inserción social de la libertad y la voluntad.
  - ¿Cómo puede llevar a cabo la libre voluntad?
    * Voluntad: abarca toda la personalidad en una relación consciente con
    el entorno en el que está comprometida.
      - Dos elementos:
        1. Puro reflejo del yo/pensamiento en general.
        2. Aspiración: la voluntad siempre se dirige a algo distinto y
        específico => querer algo.
        
    * Libertad: la unificación de la libertad objetiva y subjetiva.
      - Concepto más importante en su filosofía del D.
      
        => L objetiva: actuar libremente.
        
        => L subjetiva: actividad autónoma.
        
      => Depende en la actividad de los individuos.
        - Insostenible sin acumulación que posibilite una vista correcta y buena.
          * Libertad como un sistema recíproco de derechos.
        - La ley como consecución positiva de la voluntad.
          * Solo las personas pueden tener leyes.
          
          => Solo si se reconoce su libre voluntad.
            - Mediante la propiedad se evidencia.
              * Necesaria para la existencia.
              
La propiedad es una expresión de la personalidad
  Voluntad => Objeto => Propiedad
  ------------------
  Indivisible por ser 
  expresión de la    => Apropiación
  autodeterminación
  
  => Solo puede pertener a uno.
  
### PI

Hegel aplica su teoría de la P a la PI.
  - Aunque sus intérpretes forman una mayoría.
  
La mente tiene que externalizarse antes de la aplicación jurídica del concepto de P.
  - La externalización de la mente crea las PI.
    * Noción muy vasta.
  - En la externalización habita la imposibilidad de ser un producto de la mente.
    * Signos del lenguaje.

El creador tiene plenos DPI
  - El destino de la creación depende del creador.
    * En su alienación se pierden todos los derechos.
    
    => Paradoja de la alienación que se solventa a través de dos tipos de valores:
      1. Valor de la copia como una sola cosa.
      2. Valor del copyright como cualquier medio de copia.
      
        => Puesta en el mercado.
          - Distante a:
            1. Lo literario.
            2. Lo estético.

La obra derivada es permisible si tiene las suficientes características 
individuales del nuevo creador.
  - Pensar en un proceso práctico porque en el resto no existe un principio
  para la legislación.
    * Quiere evitar una aplicación aleatoria.
    * Se torna en una cuestión de honor.
    
      => Cuestión moral.

Argumentos a favor del tiempo limitado y el dominio público.
  - La voluntad es necesaria para la apropiación.
  - La voluntad se determina a través del uso, es activa.
  
  => Sin actividad = dominio público.
  
Dos formas de PI:
  1. P privada: mientras el autor iva, se manifiesta su voluntad.
  2. P pública: fruto de la pérdida de P privada o cuando esta es apropiación de muchos.
  
Siempre y cuando el autor no sea perjudicado por el valor secundario, los
usuarios pueden usar las copias a su gusto.
  - Valor secundario.
    1. Valor de la inversión en tiempo o en dinero.
    2. Valor de la venta y gusto del público.
    
    => Si 1, uso limitado hasta que se recupere la inversión.
      - Después se libera.
    
    => Si 2, hay un uso ambiguo que lo vuelve irrealizable.
    
### Sumario

 Noción muy extensa de PI.
   - Casi cualquier creación intelectual puede protegerse, excepto las ya conocidas.
   - La reproducción pertenece si no hay expresión distintiva.
   - Creación de PI embebida a su teoría filosófica.
   - Explica la posibilidad de comercialización sin pérdida de DPI.
   - Explica el tiempo limitado de la PI.
   - Explica el porqué pueden proteger PI que puedan formar parte de la cultura general.
   
   => vs:
     - Falla en delimitar el rango de protección.
     - Utiliza elementos anticuados como el honor.
     - Los derechos se esfuman en las obras derivadas.
     - Falla en explicar claramente el plagio y las obras derivadas.
     
     => Sin embargo, es la aproximación más completa.   

## Foucault

En Hegel, Kant y Locke existe una enorme conexión entre el autor y su obra.
  - DPI por voluntad o esfuerzo del autor.
  - Sociedad tiene un rol recundario.
  
  => Foucault:
    - Hace énfasis en el rol social.
    - Disminuye la importancia del creador en su proceso de creación.
    
Foucault no menciona PI, se concentra en su relación entre el texto y su autor.
  - Impacto sobre PI.

### Definiendo la función-autor

Dos elementos distintos:
  1. Análisis sociohistórico del autor.
    - Importante para la crítica literaria.
  2. Construcción de la función-autor.
    - Impacto jurídico.
    
  => Proceso por el cual el nombre del autor es aplicado al texto.
    - Nombre del autor como marca.
    - Caracterízación del modo de ser del discurso.
    
    => No solo indica calidad, sino una forma de cómo recibir el texto.
      - Recibimiento de cierto estatus.
      - Momento privilegiado de la individualización de las ideas.
      
La función-autor en:
  - Las ciencias: nada particular porque el trabajo depende del valor científico.
  - Las artes: el nombre del autor domina el valor del trabajo.
  
  => Nunca ha sido igual.
    - En el medievo la función-autor también aplicaba a las ciencias.
    
      => No es fijo, sino variable.
        - La función en general puede cambiar con el tiempo.
        - El estatus de un trabajo puede variar con el tiempo.
        
La función del autor es transmitir sus ideas a la sociedad.
  - Junto con la sociedad crea => La aporpiación es frágil.
  
El rol de la originalidad del autor no es más importante que el rol social.
  - La función-autor es algo que la sociedad aplica al texto.
    * No la puede aplicar el autor.
    * Es fácil de aplicar.
    
    => El autor no precede a la obra.
    
En Hegel y Locke la PI viene de la creación.
  - Foucault propone un segundo acto.
    * La función-autor que solo puede hacer la sociedad.
    
      => La sociedad define qué es la PI al aplicar la función-autor,
      aún en ausencia de leyes.
      
Texto != Obra
  - Hay textos que no se consideran obras. P. ej., una lista de compras.
  
    => También en sus límites de cuándo inicia o cuándo termina.
    
  => La individualización de las ideas como el momento de las leyes dan garantías.
    - No hay necesidad de la dicotomía idea/expresión.
      * Yace sobre la opinión caso a caso de la sociedad.
    
    => Teoría funcionalista.
    
El proceso de publicación precede a la función-autor.
  - Posibilidad de que el texto, a través de su publicación, se considere una obra.
    * El acceso público es una precondición para la función-autor.
    
El autor es poco significativo => PI es improbable.
  - En Hegel y Locke el creador siempre es dueño de lo creado aunque no existiera nadie más.
  - En Foucault es distinto: la relación está institucionalizada.
  
    => Distinta a las tradiciones de derecho:
      - Continental: centrada en el proceso de creación.
      - Anglosajona: desinteresada en el escritor como autor.
      
      => Ambas desconocen el rol social en donde el texto se convierte en obra.
        - Su dependencia es en relación con una continua existencia, no en el autor.
        - No hay conflicto con los D morales.
          * La perspectiva del autor puede cambiar la intelección social sobre la obra.
          
### Sumario

Foucault:
  - Solo habla del autor.
    * Pero es extensible a otras PI.
      - No se reconoce al dueño de una patente por su discurso, sino por la ley.
    
  - Solo se enfoca en el valor del discurso.
    * No habla sobre la compensación económica.
    
  => Probablemente no estaba pensando en PI.
    - Pero menciona que la función-autor puede tener un impacto en la apropiación.
    
  => Sus interés es en las implicaciones literarias, no jurídicas.
  
# Conclusión

En las DPI están presentes:
  - Locke: perspectiva del trabajo.
  - Hegel: aspectos morales.
  
  => Búsqueda de una integración, aunque las posturas utilitaristas y lockeanas prevalecen.
    - Pero hay problemas en la aproximación utilitarista.
      * No lidia más que con lo económico. P. ej., deja de lados los valores espirituales.
      
El concepto de PI está profundamente anclado en el entendimiento contemporáneo del mundo.
  - Imposibilidaddel control total.
  - Siempre comprometerá múltiples aproximaciones e ideas.
