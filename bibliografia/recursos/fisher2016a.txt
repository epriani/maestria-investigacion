---------------------------------------- Extensión de 100 c.----------------------------------------

PI = doctrinas legales que regulan el uso de ideas.
  - Copyright: formas originales de expresión.
  - Patentes: inventos y cierto tipo de descubrimientos.
  - Marcas registradas: palabras y símbolos que identifican bienes y servicios.
  - Secretos comerciales: información secreta.
  - Atributos de personalidad: intereses de las celebridades.

 A + relevancia económica y cultural, + interés académico.

# Estudio preliminar

Cuatro enfoques:
  1. Utilitarista: maximización del bienestar social.
    - La más empleada.
  2. Teoría del trabajo: quien trabaja para producir algo tiene un derecho natural sobre el producto
  de sus esfuezos que el Estado tiene que proteger.
    - Origen en Locke.
  3. Teorías personalistas: los productos son expresiones de las voluntades o personalidades
  del autor.
    - Origen en Kant y en Hegel.
  4. Teoría de la planificación social: fomenta una cultura más equitativa y atractiva.
    - Origen eclético: Thomas Jefferson, Marx, realistas jurídicos y republicanismo clásico.
    - Menos conocido.

# Explicando el patrón

Existe una dependencia teórica a lo realizado por jueces, legisladores y abogados.
  - Común en los utilitaristas.

Referencia a la importancia de gratificar a los creadores por su creación, al ser algo con valor.
  - Común en la teoría del trabajo.

Derechos morales derivados de Kant y Hegel, y común en Francia y Alemania: creación como
realización del creador.
  - Común en la teoría de la personalidad.

Búsqueda de una cultura más justa y consideraciones sobre el impacto del internet en la PI.
  - Común en la teoría de la planificación social.

=> Influidas por lo jurídico.
  - No es suficiente para explicarlas.
    1. Muchos casos que no han hecho eco en la teoría.
    2. En el material jurídico las teorías se mezclan.

    => Desvío entre lo teórico y los hechos jurídicos.

Propiedad en general     | PI
-------------------------|---
Utilitarismo =>          |
T. del trabajo =>        |
T. de la personalidad => |
                         | T. de la planificación social (TPS)

# Lagunas, conflictos y ambigüedades

Los enfoques pretenden:
  - Dar explicaciones sistemáticas.
  - Ser guía para legisladores y jueces.
  
  => En los hechos son menos útiles debido a sus incosistencias.

## A

Teoría utilitarista.
  - Interpretación del ideal benthamiano:

    > Mayor bien para el mayor número.

  => Maximización del bienestar según la eficiencia de Kaldor-Hicks.

    => vs:
      - No hay identidad.
      - Las funciones de utilidad con inconmensurables.
      - Existe un sesgo en la muestra en favor de los deseos de los ricos.
      - Es una definición estrecha del bienestar.

Tres opciones para el bienestar:
  1. Teoría del incentivo:
    - Busca determinar la duración óptima de una patente.

    => vs:
      - Falta de información.
      - Unos dicen que otro tipo de recompensas podrían mantener la producción en ausencia de la PI.
      - Otros dicen que no es posible otro tipo de recompensa primaria.

  2. Optimización de los patrones de productividad.
    - La PI ayuda a saber cómo orientar los esfuerzos productivos.

    => vs:
      - Falta de información para aseverar.
      - Más protección => Mayor necesidad de inversión.
      - Menos protección => Menos recompensa.

  3. Invención con rivalidad.
    - Al menos reducir la duplicación o la falta de coordinación en la actividad inventiva,
    para evitar despilfarro.
      1. Una ineficiente carrera por el bote de oro.
      2. Una ineficiente carrera secundaria al buscar una mejora rentable de algo ya existente.
      3. El desperdicio social de inventar alrededor.

    => vs:
      - La reducción del desperdicio en un aspecto, lo incrementa en otro.
      - Provocaría que en un nivel primario se busque ser el pionero.
      - Ineficiencia en un nivel secundario por la estrechez del inventor-coordinador.

  => vs:
    - No existe una teoría general.
    
    => El enfoque utilitarista está limitado.

## B

Teoría del trabajo.
  - Justificación de Locke a los derechos de propiedad
    * Duda acerca de si justifica todos los tipos de PI.
      - Tienden a ser innecesarios para la supervivencia.
      - Pueden ser usados por un sin fin de personas.
    
      => Locke no dijo nada sobre la PI.
        - Posibles interpretaciones:
          1. El tiempo y el esfuerzo => Posible intención original.
          2. La actividad involuntaria => Puede interpretarse así.
          3. La actividad socialmente beneficiosa => Puede interpretarse así.
          4. La actividad creativa => Relevante para el imaginario sobre el creador.

        => Muy diferentes entre sí.
          - Si 2 => PI solo para quien ama su trabajo.
          - Si 3 => PI solo si es socialmente beneficiosa.
          - Si 3 => PI solo si es «creativo».

          => Conflicto entre los «bienes comunales» y la PI.

    => Locke no proporciona una guía sobre los alcances de los DPI.

## C

Teoría de la personalidad.
  - Los derechos de la propiedad privada solo deberían de reconocerse cuando promueven
  el desarrollo humano.
    * Necesidad de identificar necesidades o intereses.
      1. Tranquilidad espiritual.
      2. Privacidad.
      3. Autosuficiencia.
      4. Autorrealización como ser social.
      5. Autorrealización como individuo.
      6. Seguridad y ocio.
      7. Responsabilidad.
      8. Identidad.
      9. Ciudadanía.
      10. Benevolencia.

      => vs:
        - Algunos dan poca idea de cuáles derechos proteger, debido al valor económico de la PI.
        - Da pie a conclusiones divergentes.
        
        => ¿El autor puede enajenar su derecho a controlar copias?
          1. Kant: la expresión del autor es inalienable asu personalidad. => No.
          2. Hegel: las aptitudes mentales son externas al autor. => Sí.

          => Concepciones del yo muy abstractas para cuestiones específicas.
          => Problema del fetichismo ya que no da respuesta sobre qué proteger.

## D

Teoría de la planificación social.
  - Una cultura más justa.
    * Algunos criterios:
      1. Bienestar del consumidor.
      2. Cornucopia de información e ideas.
      3. Tradición artística abundante.
      4. Justicia distributiva.
      5. Democracia semiótica.
      6. Sociabilidad.
      7. Respeto.

      => vs:
        - Involucra muchas índoles que van más allá de los DPI.

# Teoría del valor

Teorías de la personalidad y de la planificación social presentan estos problemas:
  1. Intolerantes por buscar regular el comportamiento según las teorías del bien.
  2. Paternalistas por limitar la libertad a su paradigma de lo bueno, pese al desacuerdo.

Utilitarismo y teoría del trabajo presentan estos problemas:
  1. Se orientan a argumentos económicos normativos.

  => Ojo: provoca un aura de neutralidad, objetividad y, sobre todo, de determinación.

=> Pese a sus problemas tiene valor.
  1. Ayudan a identificar soluciones no tan obvias.
  2. Fomenta valiosas conversaciones entre los legisladores.
    1. Mejor comunicación entre agencias administrativas.
    2. Mejor comunicación entre legisladores y ciudadanía.
    3. La comunicación ayuda a atender la insuficiencia de la teoría.
