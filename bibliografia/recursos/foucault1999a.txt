---------------------------------------- Extensión de 100 c.----------------------------------------

# Introducción

1. Imposibilidad de tratar al autor como definido o nombre propio.
2. El autor no es el propietario o responsable de sus textos.
  - Ni productor ni inventor.
3. Al autor se le atribuye lo escrito.
4. Posición del autor.

# Presentación

Trabajo no acabado
  - Someterlo a las objeciones.
  - Tener sugerencias.

  => ¿Qué es un autor?
    - Corrección al uso ingenuo de los autores en *Las palabras y las cosas*.
      * No describirlos correctamente.
    
      => Aunque no era su intención.

      * Generar grotescos parentescos.

      => Aunque nunca buscó un cuadro genealógico.

    => Se emplearon para medir las consecuencias de otro texto (*La arqueología del saber*)

# I

La noción de autor constituye el momento fuerte de la individuación en la historia de las ideas.
  - ¿Cómo se instauró esa categoría fundamental del nombre y la obra?

Principio ético fundamental de la escritura contemporánea.
  1. La escritura se ha librado del tema de la expresión.
  2. La obra recibe el derecho de matar.

Dos nociones importantes:
  1. «Obra»: su unidad designada es problemática.
  2. Escritura.
    - Modalidad crítica.
    - Modalidad religiosa.

    => Sentido escondido y significaciones implícitas en un sentido trascendental.

# II

Ver lo que la desaparición hace aparecer.

Nombre del autor es un nombre propio y como tal no solo indica, sino que también describe.
  - Pero no es esencialmente un nombre propio.
    * Ejerce cierto papel en relación con el discurso.

  => Permite
    1. Función clasificatoria.
    2. Interrelación de textos.
    3. Perfilar el recibimiento del escrito.

    => Autor entre estado civil y ficción: ruptura

      => Caracteriza el modo de existencia, de circulación y de funcionamiento 
      de ciertos discursos en el interior de una sociedad.

# III

¿Cómo se caracteriza un discurso portador de la función-autor?
  - Objetos de apropiación.
    * Primero como apropiación penal para castigar discursos transgresivos.
    * Después como propiedad del texto como derechos de autor a obtener beneficios.

La marca de autor no es tanto un argumento de autoridad, sino una muestra de su aprobación.
  - Según su modo, otorga un sentido.

La función-autor trata de dar un estatuto realista.
  - El autor como:
    1. Nivel constante de valor.
    2. Campo de coherencia.
    3. Unidad estilística.
    4. Momento histórico.

    => Exégesis cristiana.

    1. Misma manifestación.
    2. Las contradicciones se encuentran.
    3. Unidad pese a la evolución.
    4. Posibilidad de explicar acontecimientos.

    => Crítica moderna.

  => Características.

El texto siempre tiene elementos que remiten al escritor.
  - No es algo específico de la función-autor, pero sí más complejo.
    * Pluralidad de ego.
    * No se remite simplemente a un individuo real.

La función-autor está ligada al sistema jurídico que encierra.

# IV

Se ha limitado la noción de «autor» al autor de textos.
  - Autor discursivo.
    * El transdiscurso son los autores más allá de un libro.
      - Fundadores de discursividad.

Los fundadores producen la regla de formación de otros textos.
  - No solo hacen posible cierto número de analogías.
  - Posibilitaron diferencias mediante la apertura de un espacio distinto a ellos.
  - Tipo semejante a la fundación de cualquier cientificidad.
    * Pero la instauración de la discursividad es heterogénea a sus transformaciones.
   
      => Su validez se define en relación con la obra.

    => Se regresa a un olvido ocultado.
      - Juego: está ahí, pero no en palabras específicas, sino a través de estas.
      - El regreso tiene valor en tanto que es el texto del autor.

# V

La relación con el autor constituye una propiedad discursiva.
  - La forma de apropiación permitiría estudiar las modalidades de existencia de un discurso.
    * Forma más clara de articulación sobre relaciones sociales.

Al poner entre paréntesis al autor, se cuestiona su carácter absoluto.
  - Fundamento originario => Función variable.

El autor como principio economizador del sentido.
- vs idea tradicional del autor como fuente indefinida de significaciones.
  * El autor no precede a las obras.
  * Es una producción ideológica.

  => ¿Qué importa quien habla?
