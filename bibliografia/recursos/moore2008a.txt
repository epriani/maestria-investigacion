---------------------------------------- Extensión de 100 c.----------------------------------------

# Introducción

Tres formas para justificar la PI
  1. Personalistas.
  2. Utilitaristas.
  3. Locke.

=> Los argumentos morales,
  - no son argumentos jurídicos,
  - sí son fuertes para argumentos jurídicos.

## ¿Qué es la PI?

Propiedad no-física producto de procesos cognitivos cuyo valor se basa en una o varias ideas.
  - Los DPI se relacionan al control de las manifestaciones físicas o expresiones de la PI.
  - Los sistemas de PI (SPI) protegen DPI mediante el control de la producción y
  la distribución de PI.

En la tradición intelectual estadounidense la PI se protege con:
  1. Copyright.

  => Trabajos originales en cualquier medio tangible.
    - Derechos conferidos:
      1. Reproducción.
      2. Adaptación.
      3. Distribución de copias.
      4. Hacerlo público.
      5. Interpretarlo públicamente.

      => Pueden venderse por separado.

  2. Patentes.

  => Invención o descubrimiento de un proceso, máquina, artículo o composición de la materia útil.
    - Una forma fuerte de protección al garantizar el monopolio limitado sobre cualquier
    expresión o implementación.
    - Derechos conferidos:
      1. Manufactura.
      2. Uso.
      3. Comercialización.
      4. Autorización a terceros
    - Protege de desarrollos independientes.

  3. Secretos comerciales.

  => Cualquier fórmula, patrón, dispositivo o información que se usa en un negocio.
    - Requisitos:
      1. Tienen que dar una ventaja competitiva.
      2. Tiene que ser secreto.
    - Restricciones:
      1. Si se descubre, ya no hay protección.
      2. No excluye un desarrollo independiente.
        * Hay protección por apropiación inadecuada.

En la tradición continental la PI se protege con el Convenio de Berna.
  - Incluye la noción de derechos morales.
    * Independientes a los derechos económicos.
    * Reclama autoría vs:
      1. Distorción
      2. Mutilación o modificación.

      => Cualquier acto que perjudique la reputación.

  - Consiste en:
    1. El derecho a crear o publicar a gusto.
    2. El derecho a reclamar autoría.
    3. El derecho a prevenir deformación.
    4. El derecho a destruir el trabajo.
    5. El derecho a prohibir críticas excesivas.
    6. El derecho a prohibir cualquier injuría.

    => Protección a cualquier perjuicio no-económico.
      * Derechos relativos a su personalidad.

# Justificaciones personalistas

PI como extensión de la personalidad.
  - Hegel: la actualización externa de la voluntad requiere de la propiedad.
    * *Elementos de la filosofía del derecho*, p. 73.
  - Los individuos tienen derechos morales sobre su talento, sentimientos, carácter y experiencias.
  - El control sobre objetos físicos o intelectuales es necesario para la autorealización.
    * Mezcla del yo con el objeto.
    * Concede control sobre metas y proyectos.

PI importante en dos sentidos:
  1. En la manipulación del objeto se obtiene una medida para la libertad.
  2. La personalidad se une al objeto.

  => No es un dominio sobre el trabajo, sino sobre el ser, la personalidad.

## Dificultades

Existen al menos cuatro problemas:
  1. No es claro que se sea propietario de nuestros sentimientos, carácter o experiencias.
  2. No es claro que estas características sean expandibles.
    * Podría ser un abandono de la personalidad.
  3. La expansión no justifica la PI.
    * A lo sumo en sí solo asegura el derecho a prohibir la alteración de la obra.
  4. Hay obras en donde no hay huella de la personalidad del creador.

## Reivindicación

Al parecer hay un elemento intuitivo en la justificación personalista.
  - También apelan aotras consideraciones morales.
    * Hegel: proteger a los creadores mediante la propiedad y para el progreso social.

# Justificaciones utilitaristas

Típico del SPI anglosajón.
  - DPI produce una cantidad socialmente valiosa de PI.
  - La justificación es a un nivel sistema o institucional.
    * De manera individual no se verá tanto la utilidad social en comparación a un plano general,
    comparando a otras PI.

Para evitar la crítica de la no-rivalidad en el campo de la PI, se justifica el DPI como
incentivo para la producción.
  - La restricción aumenta el valor de una idea.

    => Incremento al incentivo.

  => De lo contrario no habrá incentivo.
    * No asegura el éxito, pero si evita el fracasosiotros acceden sin esfuerzo.

Los DPI no es porque el autor los merezca o haya mezclado su labor, sino para asegurar una
cantidad de PI para la sociedad.
  - Argumento formal:
    1. SPI syss maximiza la utlidad social.

    => Argumento que hace referencia a las teorías del bien y del derecho de manera peculiar.
      - Solo fijarse en las consecuencias para una teoría moral firme.

    2. Los DPI del SPI incentivan la creación de PI.

    => Argumento empírico según el cual el creador no tendrá incentivos si no existe
    una garantía de por medio.

    3. La creación de PI produce una cantidad óptima de progreso social.

    => Argumento donde el progreso cultural, tecnológico e industrial es necesario
    para la utilidad social.

  => SPI es necesario.

## Dificultades

Dejando de lado las críticas generales:
  - La segunda premisa puede no ser verdadera
    * Pueden existir SPI que no restrinjan el uso.

### Alternativas a las patentes

1. Una alternativa es el subsidio estatal.
  - Apoyo cuyos productos sean de propiedad pública.

    => vs: 
      - No da el suficiente incentivo que ofrece el monopolio limitado de los DPI.
      - El gobierno tiende a ser ineficiente.

2. Sistema de recompensas: el creador es pagado por el Estado según sus ventas,
mientras que lo creado pasa al dominio público.
  - Los creadores no pierden el incentivo de generar ganancias.
  - El Estado no se preocupa por quién apoyar.
  - Se evitan precios inflados, fruto del monopolio.
  - Fomenta el desarrollo inmediato de innovaciones a partir de otros o de mejoras.

3. A veces es innecesario.
  - Ser el primero puede ser un incentivo suficiente.
  - Ofrecimiento de garantías o soporte puede ser un incentivo para un negocio redituable.
  - Protección avanzada puede evitar la apropiación indeseada.

### Alternativas a los derechos de autor

1. Recompensas semejantes al apartado anterior.
2. Sistema con diferentes niveles de control que hace innecesario los derechos de autor.
3. Solo prohibir la reproducción comercial no autorizada.
4. Los derechos de autor no benefician al autor, sino a quien publica.
  - Caso principal de las publicaciones digitales.

### Secretos comerciales

Problemático para la justificación utilitarista porque:
  - Si la difusión de la información acarrea beneficios sociales,
  entonces cualquier acto de interrupción va en contra a la utilidad social.

  => Caso de los secretos comerciales.

### ¿Beneficios a largo plazo?

Es difícil determinar los costos o los beneficios de los SPI.
  - No se sabe casi nada.
  - No hay base empírica para asegurar que el progreso le es necesario los SPI.
    * Difícilmente el argumento utilitarista puede ser una justificación para los DPI.

## Reivindicación

1. Casi todas las críticas se enfocan a un problema de implementación.
  - Mejorar mediante:
    1. Restricciones a la primera venta.
    2. Limitaciones más adecuadas de los DPI.
    3. Creación de tecnologías que permiten el acceso a la vez que protegen los incentivos.

2. Cambiar los SPI es demasiado costoso.
3. Existe un elemento intuitivo
  - Si las leyes son para promover el desarrollo humano, entonces hay razones para un SPI.
    * Existe evidencia empírica que la propiedad es mejor a su ausencia.

    => Interlanilización de beneficios vs. tendencia a la disminución del valor.
      1. Fomenta la búsqueda de eficiencia.
      2. Aumenta el valor y la utilidad de los recursos naturales.
      3. Evita el derroche.
      4. Evita la pérdida de valor.
      5. Evita el secretismo característico de los gremios.

# Justificaciones lockeanas

DPI por el trabajo involucrado del autor.
  - Mezcla del trabajo con un objeto no apropiado.
  - Intuición donde quien crea, tiene derechos sobre su creación.

Argumento formal de Locke
  1. Los individuos son dueños de su cuerpo y su labor.
  2. Cuando se trabaja sobre un objeto, ya no es disociable.

    => Se generan derechos de control.
      - Expansión de derechos de la labor al objeto.

  => vs:
    1. La idea de mezcla no es coherente.
    2. Quizá sea una pérdida en lugar de expansión.
    3. Trabajos secundarios tienen la misma jerarquía a los primarios.
    4. Solo da derechos limitados, no titularidad.
    5. Problema relativo a lo que se consideran los límites de la labor.
    6. Si en la labor se emplean medios sociales,
    entonces debería pertenerle a la sociedad.

Modificación del argumento original.
  - Problema de la apropiación original.
    * La primera apropiación es solo si queda suficiente e igual de bueno para los demás (Locke).
    
    => Se entiende como condición necesaria.

    => Ahora se entenderá como condición suficiente.
      - Una cuestión es apropiarse y dejar suficiente.
      - Otra cuestión es que a partir de lo suficiente se apropie algo distinto
      a lo apropiado previamente.
        * Ej. Una pizza comprada entre varios y alguien toma un pedazo.
          1. No es su pedazo porque al apropiárselo dejó suficiente
          e igual de bueno para los demás.
          2. Sino que es su pedazo porque hay suficiente para que cada quien tenga su pedazo.
          
          => Noe s que «deje», sino que «haya» suficiente. 

      => 
        1. El trabajo intelectual tiende a ser algo voluntario que 
        puede buscarse no ser interferido.
        2. La garantía es por lo hecho o por lo desecho, no por lo dejado.
        3. Respeto a la autonomía y soberanía.

## Un principio de Pareto

Si la mayoría de los individuos se apropian y no perjundican a nadie =>
Solo una minoría que se apropia son los que perjudican.

  => Principio de no daño.

  => Puede existir un beneficio secundario.

=> Es una balanza general entre los beneficios y los prejuicios.

=> vs:
  1. Duda sobre si esto es aplicable enun mundo limitado.
    - Ganancia de unos es pérdida de otros.

    => vs:
      1. Pérdida en un aspecto pero quizá ganancia en otro.
      2. En PI el uso no excluye el uso de otros, sino que beneficia a todos (Locke correcto).

    => Posibilidad de que sea un principio de moral individualista.

## Problema de fondo

Asumiendo el principio de Pareto, cabe preguntarse:
  1. ¿Qué es lo que se entiende por perjuicio?
  2. ¿Cuáles situaciones se emplearán para comparar si alguien fue perjudicado?

    => Porblema de fondo:
      - No responde a una teoría del valor, sino a la habilidad de determinar el resultado.
      - Se dirige a un nivel individual.
        * Primera base de comparación:
          estado natural <-> estado de apropiación
        * Segunda base:
          estado de apropiación <-> segundo estado de apropiación

      => No se trata de comparar el estado previo de alguien sobre la creación de algo valioso
      y cómo estaría si lo apropiara.
        - Demasiado amplio.
        - Es:
          1. Generación del control.
          2. Asentimiento si no se perjudica a nadie.
          3. Gestación de los derechos.
        
            => Por lo tanto, no daño al gestarse la apropiación, no al negar el acceso.

# Conclusión

La justificación personalista es la más débil.
  - Poco desarrollada.
  - Una cuestión es que esté mal distorsionar el trabajo de otros,
  otra es que por ello se deban constituir PI.

La justificación utilitarista es superior.
  - Pero precisa de datos empíricos difíciles de obtener.
  - Cuenta con los recursos morales suficientes para PI.

La justificación lockeana es la más fuerte.
  - No hay espacio a la refutación, excepto que la apropiación, no su restricción posterior,
  perjudique a otros.
