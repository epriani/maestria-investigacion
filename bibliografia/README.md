# Bibliografía

La bibliografía está hecha en BibTeX, para su visualización puede 
utilizarse una interfaz gráfica como [JabRef](http://www.jabref.org/) o 
[verse directamente](https://github.com/NikaZhenya/maestria-investigacion/blob/master/bibliografia/bibliografia.bib).
