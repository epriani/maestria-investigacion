Ciudad de México, 18 de abril de 2017

**Comité Académico del** \
**Programa de Maestría y Doctorado en Filosofía,** \
**Universidad Nacional Autónoma de México**

El sector editorial ha sido una de las principales áreas de trabajo a las que 
me he dedicado desde la educación media superior. Fue por el interés en la
publicación e investigación que decidí estudiar la licenciatura en Filosofía.
Las actividades académicas y profesionales que he desempeñado siempre han
estado orientadas a la difusión de productos editoriales.

Debido a mi habilidad con el uso y creación de *software*,
dirijo mi atención a la creación y mantenimiento de sistemas que 
faciliten la compartición de la cultura y la labor editorial. «¿Qué tiene que ver 
esto con la filosofía?», es una pregunta que constantemente se me hace, y 
simplemente contesto que «todo».

La búsqueda del libre acceso a la cultura que he llevado acabo desde hace más
de una década la he visto como una manifestación de diversas inquietudes
que me orientaron al estudio de la filosofía, la cual es la relación de nosotros 
con nuestra herencia cultural. Aunque claro, lo que más rápido sale a
relucir del quehacer cultural que he desempeñado son las cuestiones técnicas
orientadas a la cultura, edición y *software* libre.

Por este motivo tengo interés en la filosofía de la cultura, ya que es también
reflexionar sobre mi actividad profesional y la de los grupos de trabajo de
los que formo parte. Dentro de este campo, el área que poco a poco me ha
intersado más son las implicaciones que la propiedad intelectual está teniendo
al momento de hacer, mantener y difundir cultura.

Esto me ha llevado a asistir desde diplomados por parte de Indautor y la 
Organización Mundial de la Propiedad Intelectual, hasta eventos de editoriales 
independientes o de activismo *hacker*. En estos eventos he encontrado 
bibliografía interesante sobre temas técnicos, políticos o socioeconómicos,
así como también ha provocado que revalore y reinterprete algunos clásicos de la 
filosofía que tuve la oportunidad de estudiar durante la licenciatura.

Además, existe un área de trabajo relativamente reciente que se llama «teoría de 
la propiedad intelectual», la cual busca justificar filosóficamente a las diversas
legislaciones que en conjunto se conocen como propiedad intelectual. Me parece un
área apasionante y preocupante. Emocionante porque paulatinamente la propiedad intelectual 
está dejando de ser un tema político-jurídico o socioeconómico para percibirse como 
un concepto que tiene profundas raíces en la manera en como entendemos nuestro mundo. 
Sin embargo, la mayoría de la bibliografía que he consultado sobre este esfuerzo 
teórico pasa por alto una serie de temas que me parecen fundamentales, como es la 
relación que tiene la propiedad intelectual con la subjetivización de la cultura, 
la tecnificación de nuestras comunidades y la predilección al sujeto creador como 
el paradigma de creación cultural.

Estos temas me parecen que permiten la vinculación del análisis filosófico
de la propiedad intelectual con las áreas de investigación que se enfocan a
la construcción de la subjetividad, filosofía de la tecnología, el vínculo
entre el autor y la obra, así como a las humanidades digitales.

Mi actividad profesional la continúo viendo la
publicación. El estudio de la maestría (aunque mi aspiración es al menos 
el doctorado) la veo como uno de los pilares que me permitirá 
darle continuidad a este desarrollo profesional (y personal).

Aunque el deseo de muchos de los que estudiamos filosofía es ser
profesor de asignaturas afines, no es secreto que es difícil encontrar 
plazas. Sin embargo, esto no me desanima, ya que mucha de mi actividad
profesional involucra impartir talleres de edición donde siempre retomo
temáticas que permiten un trato filosófico, debido a las posibilidades 
y retos que nos permiten las nuevas tecnologías de la información y la 
comunicación en el sector cultural.

En este sentido, pretendo orientar mis estudios de posgrado al área de
la investigación interdisciplinaria y bajo metodologías experimentales
en la liberación de los resultados o en el análisis de la información,
cuyas principales temáticas sean la relación entre la propiedad
intelectual y la cultura libre con la filosofía. Como puede observarse,
esto también implica darle continuidad a los intereses de difusión
cultural que implica el acceso abierto de los frutos editoriales.

Sin nada más por agregar, me despido, ojalá me den la oportunidad de estudiar
esta maestría que pretendo por tercera ocasión, pero que por cuestiones administrativas
solo me he quedado con el deseo.

_____________________________
Ramiro Santa Ana Anguiano
