# Investigación de maestría

Este es el repositorio para la investigación titulada *El creador y lo 
creado: la propiedad intelectual como supuesto en la creación cultural y 
filosófica*.

## Licencia

La investigación está bajo [Licencia Editorial Abierta y Libre (LEAL)](http://leal.perrotriste.io/).
