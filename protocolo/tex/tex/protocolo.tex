\chapter*{Título}\addcontentsline{toc}{chapter}{Título}\label{tuxedtulo}\chaptermark{Título}{}

\noindent\emph{El creador y lo creado: la propiedad intelectual como supuesto en la
creación cultural y filosófica}.

\chapter*{Planteamiento}\addcontentsline{toc}{chapter}{Planteamiento}\label{planteamiento}\chaptermark{Planteamiento}{}

\phantomsection\section*{Problema}\addcontentsline{toc}{section}{Problema}\label{problema}

\noindent El advenimiento de la era digital ha posibilitado formas de creación
cultural que cuestionan conceptos como los de

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  autoría, siendo los \emph{remix}, memes, \emph{gifs} comprensibles y
  usables sin necesidad e incluso sin posibilidad de rastrear «un»
  autor;
\item
  autoridad, donde las distintas formas de colaboración y trabajo en
  equipo en la piratería digital y la digitalización se presentan como
  fenómenos que desafían los aparatos jurídico, cultural o
  universitario;
\item
  la cultura como objeto de consumo, por medio de las licencias de uso
  que permiten la completa reutilización sin necesidad de pagar por ello
  como las licencias Creative Commons o \textsc{\lowercase{GPL}}\footnote{\hspace{1mm}\cite{gnu2017a}.}, así
  como la descarga gratuita de artículos científicos del Open
  Access\footnote{\hspace{1mm}\cite{open2016a}.} y
\item
  metodologías profesionales para la creación cultural, por la cual
  divesas comunidades en internet sin ningún tipo de certificación
  ejercen funciones de edición, revisión por pares o investigación
  académica como Epublibre, Aaaaarg, LibGen, Sci-Hub o Academia.edu.
\end{enumerate}

\noindent Esto ha provocado una crisis en el modelo cultural vigente en diversas
instituciones estatales o privadas ya que estos cuatro conceptos son
ejes primordiales para su sostenimiento.

La principal reacción ante esto ha sido el fortalecimiento de este
modelo a través de una vía técnico-jurídica por la cual se establece lo
que es «correcto», «legal» y «técnicamente viable». Como ejemplos de lo
primero tenemos las constantes campañas para el registro de obras o
invenciones en Indautor o \textsc{\lowercase{IMPI}}, la validez curricular de estas por medio
de identificadores estandarizados como el número de patente, el \textsc{\lowercase{ISBN}},
\textsc{\lowercase{ISSN}} o \textsc{\lowercase{DOI}}, o la publicación de los hallazgos o las creaciones en
organismos especializados como las publicaciones arbitradas o boletines
institucionales. De lo segundo, está patente en la búsqueda de expandir
los derechos exclusivos de obras o invenciones, como la reforma en 2003
de la Ley Federal del Derecho de Autor,\footnote{\hspace{1mm}\cite{indautor1996a}.} la
inadvertida ley Televisa\footnote{\hspace{1mm}\cite{bbc2017a}.} y la fallida iniciativa
de \textsc{\lowercase{SOPA}} mexicana, también conocida como ley
Döring;\footnote{\hspace{1mm}\cite{publimetro2017a}.} las constantes campañas
antipiratería, como los \emph{spots} en cines comerciales, radio y
televisión en donde se criminaliza la utilización indeseada de propiedad
intelectual, y los tratados internacionales cuyos ejes centrales son el
control de la propiedad intelectual como el Acuerdo Transpacífico de
Cooperación Económica o los diversos tratados administrados por la \textsc{\lowercase{OMPI}}.
Como ejemplos del tercer caso tenemos la gestión de derechos digitales
cuyo objetivo es evitar el uso indeseado de propiedad intelectual a
través de impedimentos técnicos, como la encriptación creada por Adobe
(empleados por diversas instituciones como el \textsc{\lowercase{FCE}} o la \textsc{\lowercase{UNAM}}), Apple o
Amazon para evitar la compartición externa a sus plataformas de
\emph{ebooks}, música o videos, la inclusión del \textsc{\lowercase{DRM}} como estándar para
el futuro de la \emph{web}, principalmente impulsado por
Netflix,\footnote{\hspace{1mm}\cite{eff2017a}.} o Readium \textsc{\lowercase{LCP}}, una tecnología abierta de
encriptación de obras propuesto por el European Digital Reading
Lab.\footnote{\hspace{1mm}\cite{readium2017a}.}

La consecuencia de este fortalecimiento es el constreñimiento de las
posibilidades de creación intelectual que deja afuera a toda actividad
cultural que no se apegue a dicho marco. Por ejemplo, lo «errado» que es
la falta de registro de una creación intelectual, pese a que la
normatividad jurídica establece que no es necesario para su protección
legal \emph{de facto};\footnote{\hspace{1mm}\cite{indautor1996a}.} la minimilización o el
descarte curricular de obras o invenciones que no se apegan a algún
lineamiento \textsc{\lowercase{ISO}} o que fueron divulgadas por canales «no oficiales» como
las redes sociales, \emph{blogs} o sitios P2P. La actividad «ilegal» de
cualquier acto de piratería, como la digitalización y publicación de
material ya no disponible o de difícil acceso, por ejemplo, las obras
huérfanas, descatalogadas o agotadas, o materiales gráficos y
audiovisuales solo disponibles en museos, acervos o archivos. O bien, la
«complejidad e inconveniencia técnica» que representa la creación de
repositorios descentralizados, alimentados directamente por el usuario,
mantenido por una comunidad y cuyo acceso no implique la creación de
muros de pago, bajo el supuesto argumento de una pérdida del control y
de la calidad de los materiales, cuya endeble justificación es
evidenciada por plataformas como Wikipedia, y las otras comunidades ya
mencionadas.

Este fortalecimiento y sus consecuencias contemplan un carácter
paradójico, el cual tiene un impacto directo en el presente y porvenir
de nuestra cultura. Al reaccionar conservadoramente se estipula un
paradigma de lo más «adecuado» o «pertinente» al momento de aprehender,
entender, comprender y compartir creaciones intelectuales, el cual al
menos indirectamente impide el desarrollo o el mantenimiento de gran
parte de la infraestructura cultural. Un ejemplo es la pretensión de
crear repositorios o base de datos cerrados cuyo costos de mantenimiento
o desaparición de los proveedores socavan la misma pretensión de control
en la calidad y salvaguarda de la información, como el \emph{software}
privativo adquirido por bibliotecas y acervos. Otro ejemplo son los
problemas relativos a los algoritmos desarrollados para medir la
relevancia de publicaciones arbitradas, que afecta la financiación de
estas mismas, más si son de reciente creación. Pero también existen
casos en los que la falta de ajuste a este paradigma cabe interpretarse
como discriminación, el ser obviado de apoyos institucionales o de
reconocimiento por una falta de certificación o por crear productos
culturales no estandarizados, como la gran mayoría de creaciones a
partir de la cultura popular, de la libre enseñanza o de las corrientes
contraculturales, o el no ser un autor visible por escribir en la lengua
materna o carecer de un nombre anglófono.\footnote{\hspace{1mm}\cite{fiormonte2017a}.} E
incluso de criminalización, como las demandas emprendidas por compañías
disqueras o editoriales académicas en donde destacan los casos de Aaron
Swartz, The Pirate Bay y Sci-Hub, pese a que el derecho a la información
es elemental para el «progreso de la ciencia y las
artes»\footnote{\hspace{1mm}\cite{contitucioneeuu1787a}.} tal cual como viene estipulado
en diversas legislaciones estatales que fundamentan a los derechos de
autor.

El efecto inmediato y continuo de esta decantación no solo es la
preservación de la idea de autor, autoridad, cultura de consumo o
paradigmas metodológicos, sino también la reproducción de un modelo
cultural hegemónico afín a la ideología del libre mercado y «capitalismo
global» a través de la fundamentación de la doctrina de la propiedad
intelectual cuya primordial vía es la priorización del supuesto carácter
privado del acto creativo entre el sujeto creador y el objeto creado.
Para ello, el discurso se ha enfocado principalmente en la
interpretación y reutilización de argumentos empleados por filósofos
modernos, con una perspectiva abiertamente utilitarista sobre el
desarrollo cultural y que no solo se reduce al establecimiento de pautas
económicas, sociales o política, sino también a la manera en como se
gesta, se reproduce y se funda el discuso filosófico.

\phantomsection\section*{Pregunta}\addcontentsline{toc}{section}{Pregunta}\label{pregunta}

\noindent Ante la crisis de este modelo, que prioriza al autor, la autoridad o la
creación cultural como bien de consumo, ¿qué tan válido es el supuesto
vínculo intrínseco entre el sujeto creador y el objeto creado, el cual
es fundamento del concepto de «propiedad intelectual»? ¿Qué ocurre
cuando se deja de tratar a la «propiedad intelectual» como un objeto,
también conocido como «ejecución concreta de una
idea»\footnote{\hspace{1mm}\cite{moore2014a}.}? ¿Qué impacto tiene la doctrina de la
propiedad intelectual en el quehacer cultural, principalmente en las
metodologías de investigación?

\phantomsection\section*{Tesis}\addcontentsline{toc}{section}{Tesis}\label{tesis}

\noindent La noción de la propiedad intelectual, según los teóricos clásicos de la
teoría, como Hughes, Drahos, Hettinger, Schroeder, Stengel o Shiffrin,
supone la existencia de un lazo indisoluble entre el sujeto creador y el
objeto creado. La crisis por la que atraviesa el modelo tradicional y
actualmente hegemónico del quehacer cultural, debido a las nuevas
tecnologías de la información y la comunicación, permite afirmar que
este vínculo es un mito. Es decir, que la relación creador/creación no
es un principio ontológico, como lo suponen los teóricos de la propiedad
intelectual, sino de uno de índole ideológica.

\phantomsection\section*{Hipótesis}\addcontentsline{toc}{section}{Hipótesis}\label{hipuxf3tesis}

\noindent La inquietud por justificar filosóficamente la propiedad intelectual es
un fenómeno relativamente reciente que se remonta a las décadas de los
setentas y ochentas del siglo pasado. Una gran síntesis de estos
primeros años es «The Philosophy of Intellectual Property» escrita por
Hughes. En esta se evidencia que si bien varias de las manifestaciones
de la propiedad intelectual (como las patentes o los derechos de autor)
tienen su génesis en a partir del siglo \textsc{\lowercase{XVII}}, es hasta mediados del
siglo \textsc{\lowercase{XX}} que se funda el término «propiedad
intelectual».\footnote{\hspace{1mm}\cite{stengel2004a}.}

La relevancia de este hecho se debe a que los discursos filosóficos de
Locke, los utilitaristas, Kant o Hegel empleados para fundar el concepto
de «propiedad intelectual» se han usado en conjunto, haciendo caso omiso
a sus génesis históricas, para justificar los diversos temas que
pretende cubrir la teoría de la propiedad intelectual. El principal
fundamento de estas justificaciones es la supuesta relación intrínseca
entre el sujeto creador y el objeto creado desde las perspectivas donde:

\begin{itemize}
\item
  La creación merece mérito, ya que es fruto de un esfuerzo mediante el
  trabajo, por lo que ha de ser recompensado y regulado para evitar que
  otros se beneficien a expensas del creador, cuya principal fuente es
  el \emph{Segundo Tratado sobre el Gobierno Civil} de Locke, base
  jurídica del derecho anglosajón moderno.\footnote{\hspace{1mm}\cite{moore2012a}.}
\item
  La creación necesita de incentivos, por el cual los individuos se
  motiven a crear, labor que se traducirá en progreso, beneficio o
  bienestar social, el cual da continuidad al discurso utilitarista que
  sirve de base para políticas internacionales como el Convenio de Berna
  o las recomendaciones de diversos organismos internacionales para los
  países en vías de desarrollo.\footnote{\hspace{1mm}\cite{moore2008a}.}
\item
  La creación es expresión de la voluntad o del discurso del sujeto
  creador cuyo vínculo inalienable precisa de protección ante quienes
  puedan deformar dicha expresión, cuyos antecedentes yacen en Kant y
  Hegel, y que al mismo tiempo justifican los derechos morales, común en
  las tradiciones jurídicas como las de Alemania, Francia o
  México.\footnote{\hspace{1mm}\cite{schroeder2004a}.}
\end{itemize}

\noindent Esto crea un ecosistema donde, independientemente de la tradición
jurídica de cada Estado, es posible establecer un paradigma en común de
lo «correcto», «legal», «técnicamente viable», «adecuado» y «pertinente»
del quehacer cultural, a partir de las posibilidades, límites,
ambigüedades y esclarecimientos de las justificaciones filosóficas de la
propiedad intelectual como un objeto el cual se ha de delimitar sus
posibilidades de uso.

Sin embargo, a partir del debate abierto por la piratería digital, el
Open Access o el \emph{software} libre es posible reorientar la
discusión a un terreno filosófico para evidenciar cómo la propiedad
intelectual se basa más bien en un supuesto que proviene de la relación
moderna entre el sujeto y el objeto. Este vínculo queda trastocado al
exibirse que antes, durante y después del acto creativo intervienen
otros actores, como el público, la sociedad y sus instituciones, que
difícilmente permiten un momento «íntimo» entre el sujeto creador y el
objeto creado, en donde se retoman reflexiones de Derrida, Foucault o
Bourdieu. Por este motivo cabe la posibilidad de percibir qué es lo que
ocurre cuando la propiedad intelectual deja de tratarse como un objeto.

Una de las posibilidades permite el análisis de la propiedad intelectual
como un aparato ideológico global que pretende perpetuar por una vía
técnico-jurídica el paradigma cultural que se ajusta a modelos del
«capitalismo global» y su economía de libre mercado, cuyo eje analítico
son los trabajos de Althusser e Illich. La otra posibilidad es la
indagación de la propiedad intelectual como un mito que permite la
«naturalización» o «normalización» del carácter prioritario del autor y
subsecuente «entendido» de su derecho de control sobre su creación, por
el cual se prosigue con las nociones de «muerte del autor» de Barthes y
«función-autor» de Foucault.

Gracias a esto es posible identificar que si bien este mito fue uno de
los principales ejes para la gestación cultural a partir de la invención
de la imprenta y cuyo auge se presentó a mediados del siglo \textsc{\lowercase{XX}}, siendo
esto una reflexión sobre la técnica vinculada a Heidegger, desde la
decentralización de la información producto de la tecnología digital,
este modelo ya no es capaz de adaptarse a los nuevos paradigmas
culturales. Esta inadaptación es a tal grado que la insistencia en su
prolongación ha mutado en una postura que pone en peligro el porvenir de
nuestra cultura al reducir las posibilidades de aprehensión,
entendimiento, comprensión y compartición culturales a partir de una
específica interpretación instrumental, técnica y jurídica de la
realidad.

\chapter*{Objetivos}\addcontentsline{toc}{chapter}{Objetivos}\label{objetivos}\chaptermark{Objetivos}{}

\phantomsection\section*{General}\addcontentsline{toc}{section}{General}\label{general}

\begin{itemize}
\tightlist
\item
  Evidenciar que el vínculo entre el sujeto creador y el objeto creado
  no es de índole ontológica sino ideológica.
\end{itemize}

\phantomsection\section*{Particulares}\addcontentsline{toc}{section}{Particulares}\label{particulares}

\begin{itemize}
\item
  Mostrar cómo la propiedad intelectual puede entenderse como un
  «aparato ideológico global».
\item
  Demostrar cómo la propiedad intelectual se funda en un «mito de la
  creación».
\end{itemize}

\chapter*{Relevancia}\addcontentsline{toc}{chapter}{Relevancia}\label{relevancia}\chaptermark{Relevancia}{}

\phantomsection\section*{Relevancia filosófica}\addcontentsline{toc}{section}{Relevancia filosófica}\label{relevancia-filosuxf3fica}

\noindent El debate abierto por la piratería digital, el Open Access y el
\emph{software} libre han dado la posibilidad de analizar una serie de
fenómenos sobre el quehacer cultural de nuestro tiempo desde la
filosofía de la cultura, la ética o la epistemología, siendo el primer
caso el enfoque de esta investigación, por el cual se evidencia que su
debate no solo es de un carácter político, económico, social o jurídico,
sino quizás también fundamentalmente filosófico. Es por este motivo que
al término «propiedad intelectual» se le pretende justificar a través de
dispares tradiciones filosóficas que se aglutinan para fundar un
mecanismo de control de la actividad creativa e intelectual, donde la
filosofía es utilizada con fines ideológicos.

\phantomsection\section*{Relevancia no
filosófica}\addcontentsline{toc}{section}{Relevancia no filosófica}\label{relevancia-no-filosuxf3fica}

\noindent Las alternativas a la propiedad intelectual han generado dinámicas
culturales que desafían los modelos tradicionales al crear comunidades y
desarrollar herramientas que facilitan la descentralización en la
creación y gestión de contenidos culturales. Estas dinámicas han causado
alarma a varias instituciones por lo que en diversos acuerdos y tratados
entre estados se han estipulado pautas para el control de la actividad
cultural y de la transferencia tecnológica acordes al panorama político
y económico actual, a través de la idea de que la creación intelectual
es propiedad sujeta a protección. Para el caso de América Latina, la
economía generada por la propiedad intelectual está valuada en un una
balanza negativa anual de miles de millones de dólares, afectando así la
creación de infraestructura cultural y tecnológica de la
región.\footnote{\hspace{1mm}\cite{cerlalc2015a}.}

\chapter*{Aportación original}\addcontentsline{toc}{chapter}{Aportación original}\label{aportaciuxf3n-original}\chaptermark{Aportación}{}

\noindent Los teóricos que buscan justificar la propiedad intelectual a través de
diversas corrientes filosóficas han tratado este término como un objeto.
Bajo esta misma perspectiva, los principales discursos críticos de la
propiedad intelectual han centrado sus argumentos desde contextos
políticos, sociales, económicos o jurídicos haciendo hincapié en que la
creación intelectual no es un objeto manipulable como una propiedad. En
esta investigación se tratará a la propiedad intelectual como un
«aparato ideológico» y un «mito» a partir de un contexto filosófico al
demostrar que su uso como objeto impide ver varias de las implicaciones
que esto tiene para el presente y porvenir del quehacer cultural, por lo
cual una juridicción menos restrictiva es una alternativa que se ajusta
más a nuestro contexto. Por ejemplo, el caso uruguayo y su «Ley de
\emph{software} libre y formatos abiertos en el Estado» que busca
fomentar «uso social del conocimiento» al restringir el uso de
\emph{software} y formatos digitales sujetos a propiedad intelectual en
dependencias gubernamentales.\footnote{\hspace{1mm}\cite{sledu2013a}.}

\chapter*{Índice tentativo}\addcontentsline{toc}{chapter}{Índice tentativo}\label{uxedndice-tentativo}\chaptermark{Índice tentativo}{}

\noindent Introducción. La técnica en el contexto de la \textsc{\lowercase{PI}}.

\begin{enumerate}[wide, labelwidth=!, labelindent=0pt]
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  La \textsc{\lowercase{PI}} como objeto.

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    La \textsc{\lowercase{PI}} como objeto a justificar filosóficamente.
  \item
    La \textsc{\lowercase{PI}} como objeto «incómodo» para la redifinición del carácter de la
    información.
  \item
    La \textsc{\lowercase{PI}} como objeto de índole política, socioeconómica o jurídica.
  \end{enumerate}
\item
  La \textsc{\lowercase{PI}} como aparato ideológico global.

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Reconfiguración de la \textsc{\lowercase{PI}} como fenómeno.
  \item
    La función reproductiva de la \textsc{\lowercase{PI}}.
  \item
    La \textsc{\lowercase{PI}} como realidad institucional.
  \end{enumerate}
\item
  La \textsc{\lowercase{PI}} como mito.

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Orígenes míticos de la \textsc{\lowercase{PI}} y su normalización.
  \item
    La función prescriptiva de la \textsc{\lowercase{PI}}.
  \item
    La \textsc{\lowercase{PI}} como supuesto en la creación cultural.
  \end{enumerate}
\end{enumerate}

\noindent Conclusión. La filosofía como liberación anticipada y continua del
conocimiento.

\chapter*{Cronograma}\addcontentsline{toc}{chapter}{Cronograma}\label{cronograma}\chaptermark{Cronograma}{}

\noindent El cronograma está basado en un calendario de 24 meses. La investigación
ya está disponible en \href{http://xxx.cliteratu.re}{xxx.cliteratu.re}
cuyo acceso permanecerá abierto y gratuito para consultar los avances y
el resultado final. De manera adicional se aplicarán métodos
computacionales para analizar cierta información de la investigación,
como son las fuentes bibliográficas.

\begin{longtable}[]{@{}lll@{}}
\toprule
\begin{minipage}[b]{0.12\columnwidth}\raggedright\strut
Mes\strut
\end{minipage} & \begin{minipage}[b]{0.40\columnwidth}\raggedright\strut
Actividad principal\strut
\end{minipage} & \begin{minipage}[b]{0.40\columnwidth}\raggedright\strut
Actividad secundaria\strut
\end{minipage}\tabularnewline
\midrule
\endhead
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
1\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Mejoramiento del acceso al repositorio\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Ajustes finales al protocolo\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
2\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Desarrollo del método computacional\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Ajustes al acceso del repositorio\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
3\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Ejecución del análisis computacional\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Corrección de errores en el método\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
4-6\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Análisis de datos\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Incorporación de nueva bibliografía\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
7-10\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Desarrollo de la primera parte\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Ajustes a la bibliografía\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
11-14\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Desarrollo de la segunda parte\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Correcciones de la primera parte\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
15-18\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Desarrollo de la tercera parte\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Correcciones de la segunda parte\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
19-21\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Desarrollo de la conclusión e intro.\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Correcciones de la tercera parte\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
22-23\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Correcciones y ajustes finales\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Mantenimiento a los archivos \emph{web}\strut
\end{minipage}\tabularnewline
\begin{minipage}[t]{0.12\columnwidth}\raggedright\strut
24\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Creación del \textsc{\lowercase{PDF}}, \textsc{\lowercase{EPUB}} y \textsc{\lowercase{MOBI}}\strut
\end{minipage} & \begin{minipage}[t]{0.40\columnwidth}\raggedright\strut
Mantenimiento del repositorio\strut
\end{minipage}\tabularnewline
\bottomrule
\end{longtable}

\chapter*{Bibliografía}\addcontentsline{toc}{chapter}{Bibliografía}\label{bibliografuxeda}\chaptermark{Bibliografía}{}

\nocite{althusser1970a,arkaute2008a,arteaga2008a,barlow2016a,barron2012a,barthes1968a,boeta2008a,bollier2016a,bourdieu2008a,bravo2005a,breakey2010a,child1990a,derrida2002a,htec1990a,htec1991a,htec1991b,eff2017a,drahos1996a,eco2016a,fiormonte2017a,fisher2016a,foucault1999a,gilmore2004a,hall2009a,hall2016a,hall2012a,hall2009b,hall2016b,hegel2005a,heidegger1994a,hettinger1989a,himma2006a,hughes1988a,illich1985a,jimenez2008a,kant2005a,kuflik1989a,lenk2007a,lessig2009a,lessig2011a,lessig2001a,lessig2012a,lever2012a,levy2016a,locke2006a,lopez2008a,mcleod2009a,ming2016a,moore2014a,moore2008a,moore2012a,nivon2016a,ortmann2016a,palmer1990a,papatheo2016a,rangel2016a,raymond2016a,rendueles2016a,ringenbach2016a,rojo2016a,rose1993a,sanchez2016a,schroeder2004a,shiffrin2007a,stallman2004a,stallman2016a,stengel2004a,indautor1996a,cerlalc2015a,open2016a,wayner2000a,sam2010a,htec1988a,htec1988b,yudice2008a,gnu2017a,bbc2017a,publimetro2017a,readium2017a,contitucioneeuu1787a,sledu2013a}

\phantomsection\addcontentsline{toc}{section}{Primaria}
\printbibliography[title={Primaria},heading=subbibintoc,heading=subbibliography,keyword={Primaria}]

\phantomsection\addcontentsline{toc}{section}{Secundaria}
\printbibliography[title={Secundaria},heading=subbibintoc,heading=subbibliography,keyword={Secundaria}]