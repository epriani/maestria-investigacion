Borrón y cuenta nueva, antes véase => 
http://inundata.org/2012/06/01/markdown-and-the-future-of-collaborative-manuscript-writing/

Para la segunda redacción, antes véase => 
https://i.giphy.com/3rgXBQIDHkFNniTNRu.gif

# Título

Filosofía pirata: la propiedad intelectual como supuesto en la creación 
cultural y filosófica
 
# Problema

## Primera redacción

El advenimiento de la era digital ha posibilitado formas de creación 
cultural que cuestionan el modelo (predigitales) en que se sostienen 
conecptos como los de autoría,  autoridad, la labor profesional (ete no 
le entiendo) y la cultura de consumo (o el consumo de la cutura, o la 
cultura como objeto de consumo?). Ante la crisis en estos modelos, se ha 
comenzado eaccionado fortaleaciendo el modelo su fortalecimiento a 
través de una vía jurídica y técnica por la cual se establece lo que es 
«correcto», «legal» y «técnicamente viable» en el quehacer cultural 
(esta idea no se enteinde muy bien, pues tienes que rematarla con una 
consecuencia: para que...), cuyas justificaciones provienen de Locke, 
Hegel, Kant y el utilitarismo. Esto tiene un impacto directo en el 
presente y porvenir de nuestra cultura ya que estipula un paradigma de 
lo más «adecuado» o «pertienente» al momento de aprehender, entender, 
comprender y compartir creaciones intelectuales, que en la mayoría de 
los casos discrimina e incluso criminaliza formas de creación 
periféricas que fundan su quehacer en una red de sujetos que 
intercambian información de manera menos estratificada y sin tantos 
intermediarios, y que en la mayoría de los casos también son críticas 
ante estos modelos hegemónicos de quehacer cultural. Aquí hay que ser 
más crudo: al reaccionar conservadoramente, se busca preservar la idea 
de autor, autoridad, profesionalismo, que aun domina nuestro quehacer 
cultural, y se marginalizan otras conepciones de conocimiento y 
cultura.. Puedes extenderte un poco mas en esta consecuencia. 

## Segunda redacción

El advenimiento de la era digital ha posibilitado formas de creación 
cultural que cuestionan el modelo en que se sostienen conceptos como los 
de autoría, autoridad, la cultura como objeto de consumo y metodologías 
profesionales para la creación cultural (edición de libros, revisión por 
pares, investigación académica, etc.). Ante la crisis de este modelo, 
este se ha fortalecido a través de una vía técnico-jurídica por la cual 
se establece lo que es «correcto», «legal» y «técnicamente viable» en el 
quehacer cultural, cuya consecuencia es el constreñimiento de las 
posibilidades de creación intelectual que deja afuera a toda actividad 
cultural que no se apegue a dicho marco. Esto tiene un impacto directo 
en el presente y porvenir de nuestra cultura, ya que al reaccionar 
conservadoramente se estipula un paradigma de lo más «adecuado» o 
«pertinente» al momento de aprehender, entender, comprender y compartir 
creaciones intelectuales, que por lo general discrimina o criminaliza 
formas de creación periféricas que fundan su quehacer en una red 
descentralizada de sujetos que intercambian información. El efecto 
inmediato y continuo de esta decantación no solo es la preservación de 
la idea de autor, autoridad, cultura de consumo o arquetipos 
metodológicos, sino también la reproducción de un modelo cultural 
hegemónico afín a la ideología tecnócrata del libre mercado y 
«capitalismo global».

Esta redacción tiene un problema. Comienzas con el advenimiento de la 
era digital y lo que posivilitas... y luego hablas de la crisis del 
modelo, sin decir cuál es el modelo antes... 
Evita términos como ideología tecnócrta que no quieren decir nada en 
concreto. Capitalismo global está bien. 

# Pregunta

## Primera redacción

¿Qué está detrás, es decir, cuál es la justificación filosófica, de este 
endurecimiento de los modelos cuando desde otra perspectiva filosófica 
se muestra como un fenómeno que *supone* que el valor económico de la 
creación intelectual dentro del «libre» mercado es la característica 
fundamental para el mantenimiento y prolongación de la infraestructura 
cultural?

A la pregunta le falta un poco. Me parece que quizás, más que irte por 
la justicaciòn "filosófica", pordrías quizás invertir la idea: 
    
    Ante la evidencia de una crisis del modelo de autor y de autoridad, 
que depende una concpeción del derecho de autor cuyos fundamentos pueden 
encontrars en algunos filósofos modernos, ¿sobre qué bases se puede 
construir un modelo alternativo, incluyente, que sea ultil para le 
porvenir? 
    
    Dale vueltas a esto que te señalo y busca la forma de enunciar el 
problema que a ti te interesa. Es un ¿crítica de la razón de autoridad 
en el modelo de autor? ¿Es la busqueda de un paradigma nuevo? Elige uno 
de los dos para centrarte. 

## Segunda redacción

Ante la crisis en este modelo, cuyas metodologías priorizan al autor, la 
autoridad o la creación cultural como bien de consumo y que ha 
constituido la doctrina de la propiedad intelectual a través de Locke, 
Hegel, Kant o los utilitaristas, ¿de qué manera este paradigma puede 
someterse a una crítica de la razón a partir del debate abierto por la 
piratería digital, el Open Access y el *software* libre, que a su vez 
sea afín a modelos culturales digitales no tecnócratas?

Me cambiaste por completo la pregunta y creo todavía no logramos 
aterrizala. Si ves, tu pregunta es como someter un paradigma a una 
crítica... ¿Es eso quieres hablar, de cómo se somete un paradigma? La 
redacción confunde. A ver, de nuevo, ¿qué preguntas te provoca el modelo 
actual de PI? Porque cuando quieres hacer una crítica, es que puedes 
preguntar cosas que ponen en cuestión en modelo. Por ejemplo: ¿es un 
vinculo natural el de la autor y la obra? ¿Toda obra intelectual debe 
ser considerada un producto? Suguiero que te enfoques en eso. 



# Tesis

## Primera redacción

La relevancia y control del valor económico de la creación intelectual, 
también conocido como derechos de la propiedad intelectual, es el punto 
de partida para hacer de la creación una «propiedad» y cuya principal 
fundamentación es la relación intrínseca entre el sujeto creador y 
objeto creado desarrollada por Hegel y Kant. A partir de ahí de percibe 
que semejante relación es un *supuesto* que fundamenta un aparato 
ideológico interestatal, también conocido como doctrina de la propiedad 
intelectual, a partir de un mito fruto de la modernidad donde el creador 
es una condición necesaria para la creación intelectual.

## Segunda redacción

La relevancia y control del valor económico de la creación intelectual, 
también conocido como derechos de la propiedad intelectual, es el punto 
de partida para hacer de la creación una «propiedad» y cuya principal 
fundamentación es la relación intrínseca entre el sujeto creador y 
objeto creado desarrollada por Hegel y Kant. A partir de ahí de percibe 
que semejante relación es un *supuesto* que fundamenta un aparato 
ideológico interestatal, también conocido como doctrina de la propiedad 
intelectual, a partir de un mito fruto de la modernidad donde el creador 
es una condición necesaria, y no solo suficiente, para la creación 
intelectual.

# Hipotesis

## Primera redacción

Las justificaciones lockeana, utilitarista o helegiana-kantiana 
establecen un paradigma de lo «correcto», «legal», «técnicamente 
viable», «adecuado» y «pertinente» del quehacer cultural basados 
principalmente en los modelos de autoría, autoridad, labor profesional y 
cultura de consumo que fueron creados en un contexto predigital. Estas 
justificaciones tienen su principal fundamento en la relación intrínseca 
entre el sujeto creador y el objeto creado por el cual se genera una 
función rectora donde el creador es una condición necesaria para la 
intelección de la creación. Sin embargo, la creación cultural en un 
contexto primordialmente digital revelan que el creador es, a lo sumo, 
una condición suficiente para la creación; es decir, que la intelección 
de la creación no precisa de un creador como eje fundacional de 
significa y de sentido. Esto muestra que la justificación que torna a la 
creación en una «propiedad» se basa principalmente en un supuesto 
moderno donde el creador tiene una particular relevacia en el acto 
creativo cuando este fenómeno es más complejo porque involucra antes, 
durante y después una serie de actores como lo son el creador, el 
público, la sociedad y sus institucionales. A partir de esta 
complejidad, cabe la posibilidad de distinguir que la función creadora 
depende directamente de un mito «creacional» en el que el acto creativo 
se reduce a la relación sujeto creador y objeto creado que 
posteriormente se pone en relación con el público, la sociedad y sus 
instituciones. De esta manera se pretende justificar un aparato 
ideológico interestatal que pretende perpetuar un paradigma cultural que 
se ajusta a los modelos técnicos, económicos, sociales y políticos de lo 
que se conoce como «capitalismo global» y su economía del «libre» 
mercado. Por ello, todo esto arroja la importancia ética, epistemológica 
y ontológica del estatus de la creación intelectual como propiedad, ya 
que tiene un impacto directo en las formas de quehacer científico, 
artístico o filo
sófico sea dentro o fuera de centros culturales, como las universidades.

Aun es confusa. Creo que en cuento resuelvas bien el problema está parte 
será más clara. 

## Segunda redacción

Las justificaciones lockeana, utilitarista o helegiana-kantiana de los 
derechos de propiedad intelectual establecen un paradigma de lo 
«correcto», «legal», «técnicamente viable», «adecuado» y «pertinente» 
del quehacer cultural. Estas justificaciones tienen su principal 
fundamento en la relación intrínseca entre el sujeto creador y el objeto 
creado, sea por el trabajo invertido o por la originalidad de la 
expresión, y por el cual se prioriza al autor como condición necesaria 
para la intelección y control de la creación. Sin embargo, en la 
creación cultural en un contexto primordialmente digital el creador rara 
vez es una figura de autoridad que da significado y sentido a la 
creación, e incluso en algunos casos el autor ni siquiera es 
identificable pese a la suma originalidad de la creación, sin que esto 
afecte la inteligibilidad de la creación. Esto evidencia que las 
justificaciones que tornan a la creación intelectual en «propiedad» se 
basan más bien en un supuesto que proviene de la relación moderna entre 
el sujeto y el objeto, cuya simplicidad queda trastocada al exibirse que 
antes, durante y después del acto creativo intervienen otros actores, 
como el público, la sociedad y sus instituciones, que difícilmente 
permiten un momento «íntimo» entre el sujeto creador y el objeto creado. 
A partir de esta complejidad, cabe la posibilidad de distinguir a este 
supuesto acto íntimo como una función creadora que pretende independizar 
y dar primacía a la relación sujeto-objeto en comparación a los demás 
actores. De esta función pueden distinguirse dos dimensiones. Una es la 
que se funda como un aparato ideológico interestatal, también conocido 
como doctrina de la propiedad intelectual, que pretende perpetuar por 
una vía técnico-jurídica el paradigma cultural que se ajusta a modelos 
del «capitalismo global» y su economía de libre mercado. La otra 
dimensión es de carácter mítico que da énfasis al autor a partir del 
supuesto vínculo intrínseco entre el creador y la creación, para así 
continuar la «naturalización» o «normalización» de esta concepción como 
un supuesto epistemológico del sentido común. Gracias a esta 
diferenciación es posible identificar que si bien la función creadora 
fue uno de los principales ejes para la gestación cultural a partir de 
la invención de la imprenta y el pensamiento renacentista, desde la 
decentralización de la información producto de la tecnología digital 
este modelo ha sido incapaz de adaptarse a los nuevos paradigmas 
culturales, a tal grado que la insistencia en su prolongación ha mutado 
en una postura tecnócrata que pone en peligro el porvenir de nuestra 
cultura al discriminar o criminalizar muchas de las actuales prácticas 
de aprehensión, entendimiento, comprensión y compartición culturales.

# Metodología

## Primera redacción

A partir del debate entre la piratería digital, el Open Access y el 
*software* libre o de código abierto, y las tres justificaciones 
filosóficas de la propiedad intelectual es posible conformar un eje de 
análisis que muestre que el centro de la discusión es una cuestión 
filosófica sobre el estatus ético, epistemológico y ontológico de las 
implicaciones de tratar a la creación intelectual como propiedad a 
partir de la examinación de la relación intrínseca entre el sujeto 
creador y el objeto creado. Esta perspectiva ha empezado a ser trabajada 
por lo que se conoce como «filosofía pirata», concepto propuesto en la 
revista *Culture Machine*, desde un enfoque de las implicaciones éticas 
de esta confrontación. Aquí se trataría además como un eje metodológico 
por el que paulatinamente se deje el terreno de la discusión 
socioeconómica, tecnopolítica y jurídica que implica la propiedad 
intelectual hacia un campo filosófico en el que se cuestiona los modelos 
de autoría, autoridad, práctica profesional y cultura de consumo a 
partir de autores como Heidegger, Derrida, Foucault, Althusser, Illich y 
Barthes. 

la metodología debe ser, por ejemplo: una herméutica, una revisión 
historiaca y crítica. Lo que pones es más bien, tu referencia academica, 
no una metodología. Pero vamos por partes, acabemos primero la pregunta.  

> Estoy de acuerdo con que quizá no es buena idea valerme de la 
«filosofía pirata» porque no es un concepto conocido y si bien esta 
perspectiva de análisis la había empezado a trabajar desde antes de 
conocer el término, me parece que es deshonesto no mencionarlo. Por otro 
lado, pienso que es pertinente para evitar *forks* de manera 
innecesaria, ya que veo que el concepto me puede ayudar a vincular el 
carácter filosófico de este debate a partir de otros autores y filósofos 
contemporáneos. Como sea, veo que según la metodología que quiero 
emplear, de todas maneras me veré forzado a justificarla filosóficamente 
(probablemente en la introducción) independientemente de que use el 
término o no, así que por economía y capacidad de vinculación, creo que 
**quizá** el uso de «filosofía pirata» es lo más pertienente, pero ya me 
dirás, jaja.





